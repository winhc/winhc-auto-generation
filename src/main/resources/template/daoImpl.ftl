package ${hibernate.classPackage};

import ${dao.importName};
import com.winhc.mis.pojo.${model.className};
import com.winhc.mis.pojo.${model.className}Example;
import org.apache.commons.lang.StringUtils;
import com.winhc.mis.PageJdbcTemplate.PageBean;
import com.winhc.mis.PageJdbcTemplate.PageQueryHelper;
import com.winhc.mis.mapper.${model.className}Mapper;
import org.springframework.stereotype.Service;
import java.util.List;
import javax.annotation.Resource;
import ${qo.importName};


/**
* <${hibernate.describe}>
* @auth:DONNIE
*/
@Service
public class ${hibernate.className} implements ${dao.className} {
	
	@Resource
    private ${model.className}Mapper ${model.classNameAttr}Mapper;
    
    @Resource
	private PageQueryHelper pageQueryHelper;
    
	
	@Override
	public ${model.className} save(${model.className} obj) {
		// TODO Auto-generated method stub
		${model.classNameAttr}Mapper.insertSelective(obj);
		return obj;
	}

	@Override
	public ${model.className} update(${model.className} obj) {
		// TODO Auto-generated method stub
		${model.classNameAttr}Mapper.updateByPrimaryKeySelective(obj);
		return obj;
	}

	@Override
	public void delete(${model.primaryKey.javaTypeName} ${model.primaryKey.attrName}) {
		// TODO Auto-generated method stub
		${model.classNameAttr}Mapper.deleteByPrimaryKey(${model.primaryKey.attrName});
	}

	@Override
	public List<${model.className}> query(${qo.className} obj) {
		// TODO Auto-generated method stub
		${model.className}Example example=new ${model.className}Example();
		${model.className}Example.Criteria criteria=example.createCriteria();

		<#list model.columnsList as column>
		<#if column.javaTypeName == 'String'>
		if(StringUtils.isNotBlank(obj.get${column.domainName}())){
    		criteria.and${column.domainName}EqualTo(obj.get${column.domainName}());
    	}
    	<#else>
    	if(obj.get${column.domainName}() != null ){
    		criteria.and${column.domainName}EqualTo(obj.get${column.domainName}());
    	}
		</#if>
 		</#list>
 		example.setOrderByClause(" ${model.primaryKey.column_name} desc ");
 		List<${model.className}> list =${model.classNameAttr}Mapper.selectByExample(example);
		return list;
	}
	
	@Override
	public ${model.className} query(${model.primaryKey.javaTypeName}  ${model.primaryKey.attrName}) {
		// TODO Auto-generated method stub
		
		return ${model.classNameAttr}Mapper.selectByPrimaryKey(${model.primaryKey.attrName});
	}
	
	
	@Override
	public PageBean<${model.className}> queryAll(${qo.className} dto) {
		// TODO Auto-generated method stub
		StringBuffer sb = new StringBuffer("select * from ${model.primaryKey.table_name} as ${model.classNameAttr} where 1=1 ");
		
        StringBuffer conditionSB = new StringBuffer();
        
        String startDate = dto.getStartDate();
        String endDate = dto.getEndDate();
        
    <#list model.columnsList as column>
    	<#if '${column.javaTypeName}' == 'String'>
    	String ${column.attrName} = dto.get${column.domainName}();
    	if(StringUtils.isNotEmpty(${column.attrName})) {
              conditionSB.append(" and ${model.classNameAttr}.${column.column_name} =");
              conditionSB.append("'");
              conditionSB.append(${column.attrName});
              conditionSB.append("'");
        }
        <#elseif '${column.javaTypeName}' == 'Integer'>
        Integer ${column.attrName} = dto.get${column.domainName}();
        if(${column.attrName} != null) {
              conditionSB.append(" and ${model.classNameAttr}.${column.column_name} =");
              conditionSB.append(${column.attrName});
        }
        <#elseif '${column.javaTypeName}' == 'Long'>
        Long ${column.attrName} = dto.get${column.domainName}();
        if(${column.attrName} != null) {
              conditionSB.append(" and ${model.classNameAttr}.${column.column_name} =");
              conditionSB.append(${column.attrName});
        }
		<#elseif '${column.javaTypeName}' == 'Date'>
		if(StringUtils.isNotEmpty(startDate)) {
              conditionSB.append(" and ${model.classNameAttr}.${column.column_name} >=");
              conditionSB.append("'");
              conditionSB.append(startDate.replaceAll("-", "").trim());
              conditionSB.append("'");
        }
        if(StringUtils.isNotEmpty(endDate)) {
              conditionSB.append(" and ${model.classNameAttr}.${model.column_name} <=");
              conditionSB.append("'");
              conditionSB.append(endDate.replaceAll("-", "").trim());
              conditionSB.append("'");
        }
		</#if>
  </#list>
        conditionSB.append("  order by ${model.classNameAttr}.${model.primaryKey.column_name} desc ");
        String sql = sb.append(conditionSB).toString();
        PageBean<${model.className}> list = pageQueryHelper.query(sql, dto.getPageSize(),
                dto.getPageNum(), ${model.className}.class);
		return list;
	}

	
}
