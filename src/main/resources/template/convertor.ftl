package ${convertor.classPackage};

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import ${vo.importName};
import com.winhc.mis.pojo.${model.className};

@Component
public class ${model.className}Convoter {
    Log log = LogFactory.getLog(getClass());

    public ${vo.className} convertorVO(${model.className} ${model.classNameAttr}) {
        ${vo.className} vo = new ${vo.className}();
        if (${model.classNameAttr} == null) {
            return vo;
        }
        try {
        	BeanUtils.copyProperties(vo,${model.classNameAttr});
            
        <#list vo.queryParams as column>
        <#if 'String' == '${column.javaTypeName}'>
			if(StringUtils.isNotEmpty(vo.get${column.domainName}())){
            	vo.set${column.domainName}Desc("");
            }
    	<#else>
    		if (vo.get${column.domainName}() != null) {
                vo.set${column.domainName}Desc();
            }
		</#if>
  		</#list>

        } catch (Exception e) {
            log.error("", e);
        }
        return vo;
    }

    public List<${vo.className}> convertorVOList(List<${model.className}> sourceList) {
        List<${vo.className}> targetList = new ArrayList<${vo.className}>();
        if (sourceList == null || sourceList.size() == 0) {
            return targetList;
        }
        for (${model.className} y : sourceList) {
            targetList.add(convertorVO(y));
        }
        return targetList;
    }

}
