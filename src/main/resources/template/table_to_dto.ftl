package ${qo.classPackage};

import com.winhc.mis.pojo.${model.className};
import io.swagger.annotations.ApiModelProperty;

/**
* <${qo.describe}>
* @auth:DONNIE
*/
public class ${qo.className} extends ${model.className} {
	/**
	 * ${qo.describe}
	 */
	 
	private String startDate;

    private String endDate;
    
    @ApiModelProperty(value="页数",name="pageNum",example="1")
    private String pageNum;
    
    @ApiModelProperty(value="单页数量",name="pageSize",example="10")
    private String pageSize;
    
    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
    
    public String getPageNum() {
        return pageNum;
    }

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }
}