package ${controller.classPackage};

import javax.annotation.Resource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.winhc.mis.common.BaseAction;
import com.winhc.mis.common.annotation.OperationLogRegister;
import com.winhc.mis.entity.dto.ResponseDTO;
import com.winhc.mis.pojo.${model.className};
import ${qo.importName};
import ${api.importName};
import ${service.importName};
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "${model.className}Controller", description = "")
@RequestMapping(value = "${model.classNameAttr}s")
@RestController
public class ${model.className}Controller extends BaseAction {
    Log log = LogFactory.getLog(${model.className}Controller.class);

    @Resource
 	private ${api.className} ${api.classNameAttr};

  
    /**
     * 查询
     * 
     * @return
     */
    @ApiOperation(value = "${ControllerName}查询", notes = "根据${qo.className}条件查询所有${ControllerName}")
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public ResponseDTO getList(${qo.className} requestDTO) {
        log.info("查询${ControllerName}：" + JSON.toJSONString(requestDTO));
 
        ResponseDTO responseDTO = ${api.classNameAttr}.queryAll(requestDTO); 
        return responseDTO;
    }

    /**
     * 增加
     * 
     * @return
     */
    @ApiOperation(value = "增加${ControllerName}", notes = "根据${model.className}对象增加${ControllerName}")
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseDTO save(@RequestBody ${model.className} ${model.classNameAttr}) {
        log.info("根据${model.className}对象增${ControllerName}:" + JSON.toJSONString(${model.classNameAttr}));
        ResponseDTO responseDTO =${api.classNameAttr}.save(${model.classNameAttr});
        return responseDTO;
    }

    /**
     * 查询
     * 
     * @return
     */
    @ApiOperation(value = "${ControllerName}查询", notes = "根据id查询${ControllerName}")
    @RequestMapping(value = "/{${model.primaryKey.attrName}}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseDTO get(@PathVariable("${model.primaryKey.attrName}") ${model.primaryKey.javaTypeName} ${model.primaryKey.attrName}) {
    	ResponseDTO responseDTO = new ResponseDTO();
        responseDTO.setBody(JSON.toJSONString(${api.classNameAttr}.queryById(${model.primaryKey.attrName}), SerializerFeature.WriteMapNullValue));
        responseDTO.setIsSuccess("T");
		responseDTO.setMsg("查询成功");
		responseDTO.setCode("00");
        return responseDTO;
    }

    /**
     * 更新
     * 
     * @return
     */
    @ApiOperation(value = "修改${ControllerName}", notes = "根据${model.className}对象的变化字段修改${ControllerName}")
    @RequestMapping(value = "/{${model.primaryKey.attrName}}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseDTO update(@PathVariable("${model.primaryKey.attrName}") ${model.primaryKey.javeTypeName} ${model.primaryKey.attrName}
    				, @RequestBody ${model.className} ${model.classNameAttr}) {

        log.info("修改${ControllerName}:" + JSON.toJSONString(${model.classNameAttr}));
        ${model.classNameAttr}.set${model.primaryKey.domainName}(${model.primaryKey.attrName});
        ResponseDTO responseDTO =${api.classNameAttr}.update(${model.classNameAttr});
        return responseDTO;
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @ApiOperation(value = "删除${ControllerName}", notes = "根据${model.className}对象删除${ControllerName}")
    @RequestMapping(value="/{${model.primaryKey.attrName}}",method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseDTO deleteProjectManageInfo(@PathVariable("${model.primaryKey.attrName}") ${model.primaryKey.javaTypeName} ${model.primaryKey.attrName}) {
        log.info("根据id删除${ControllerName}:" + ${model.primaryKey.attrName});
        ResponseDTO responseDTO =${api.classNameAttr}.delete(${model.primaryKey.attrName});
        return responseDTO;
    }

  
}
