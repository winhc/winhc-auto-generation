package ${api.classPackage};

import com.winhc.mis.pojo.${model.className};
import com.winhc.mis.common.GenericServiceManage;
import ${vo.classPackage}.${model.className}VO;

/**
* <${api.describe}>
* @auth:DONNIE
* @see: 
*/
public interface ${api.className} {

		/**
	 * 保存
	 * @param vo
	 * @param responseDTO
	 * @return
	 */
	 ResponseDTO save(${model.className} vo);
	
	 	/**
		 * 修改
		 * @param vo
		 * @param responseDTO
		 * @return
		 */
	 ResponseDTO update(${model.className} vo);

	 	/**
		 * 删除
		 * @param t
		 * @param responseDTO
		 * @return
		 */
	 ResponseDTO delete(${model.primaryKey.javaTypeName} id);	
	 
	 /**
	  * 查询单个  
	  * @param t
	  * @param responseDTO
	  * @return
	  */
	 ${vo.className} queryById(${model.primaryKey.javaTypeName} id);	

	
}
