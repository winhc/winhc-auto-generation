package ${vo.classPackage};

<#assign  hasDateField="false"/>
<#assign  hasDecimalField="false"/>
<#list vo.columnsList as column>
    <#if column.javaTypeName == "Date">
        <#assign  hasDateField="true"/>
    </#if>
    <#if column.javaTypeName == "BigDecimal">
        <#assign hasDecimalField="true"/>
    </#if>
</#list>
<#if hasDateField=="true">
import java.util.Date;
</#if>
<#if hasDecimalField=="true">
import java.math.BigDecimal;
</#if>
import com.winhc.mis.pojo.${model.className};

/**
* <${vo.describe}>
* <功能详细描述>
*
* @auth:DONNIE
*/
public class ${vo.className} extends ${model.className} {
    /**
    * ${vo.describe}
    */

  <#-- table domain 类属性  -->
  <#list vo.queryParams as column>
  	/*** ${column.column_comment} ***/
	private String ${column.attrName}Desc;
  </#list>

  <#-- table domain 类属性 getter setter  -->
  <#list vo.queryParams as column>

	public String get${column.domainName}Desc(){
		return ${column.attrName}Desc;
	}

	public void set${column.domainName}Desc(String ${column.attrName}Desc){
		this.${column.attrName}Desc = ${column.attrName}Desc;
	}
  </#list>
 }