package ${model.classPackage};

<#assign  hasDateField="false"/>
<#assign  hasDecimalField="false"/>
<#list model.columnsList as column>
  	<#if column.javaTypeName == "Date">
  		<#assign  hasDateField="true"/>
	</#if>
	<#if column.javaTypeName == "BigDecimal">
	    <#assign hasDecimalField="true"/>
	</#if>
</#list>
<#if hasDateField=="true">
import java.util.Date;
</#if>
<#if hasDecimalField=="true">
import java.math.BigDecimal;
</#if>
import com.winhc.mis.common.BaseJSON;

/**
* <${model.describe}>
* @auth:DONNIE
*/
public class ${model.className} extends BaseJSON{
	/**
	 * ${model.describe}
	 */

  <#-- table domain 类属性  -->
  <#list model.columnsList as column>
  	private ${column.javaTypeName} ${column.attrName};
  </#list>

  <#-- table domain 类属性 getter setter  -->
  <#list model.columnsList as column>

	public ${column.javaTypeName} get${column.domainName}(){
		return ${column.attrName};
	}

	public void set${column.domainName}(${column.javaTypeName} ${column.attrName}){
		this.${column.attrName} = ${column.attrName};
	}
  </#list>
}