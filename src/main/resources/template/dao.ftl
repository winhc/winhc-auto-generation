package ${dao.classPackage};

import com.winhc.mis.common.GenericDaoManage;
import com.winhc.mis.pojo.${model.className};
import ${qo.classPackage}.${model.className}DTO;

/**
* <${dao.describe}>
* @auth:DONNIE
*/
public interface ${dao.className} {
	/**
     * 新增
     * 
     * @param ${model.className}
     * @return
     */
    ${model.className} save(${model.className} t);

    /**
     * 根据updateByPrimaryKeySelective修改
     * 
     * @param ${model.className}
     * @return
     */
    ${model.className} update(${model.className} t);

    /**
     * 根据主键删除
     * 
     * @param id
     */
    void delete(${model.primaryKey.javaTypeName} id);

    /**
     * 根据selectByExample查询
     * 
     * @param t
     * @return
     */
    List<${model.className}> query(${qo.className} t);

    /**
     * 根据主键id查询,若id类型为Long/Integer，则参数为 id+""
     * 
     * @param id
     * @return
     */
    ${model.className} query(${model.primaryKey.javaTypeName} id);

    /**
     * 返回PageBean的查询方法，pageQueryHelper
     * 
     * @param dto
     * @return
     */
    PageBean<${model.className}> queryAll(${qo.className} dto);
}
