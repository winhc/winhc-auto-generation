package ${service.classPackage};


import ${api.importName};
import ${vo.importName};
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.winhc.mis.PageJdbcTemplate.PageBean;
import com.winhc.mis.entity.dto.ResponseDTO;
import com.winhc.mis.pojo.${model.className};
import ${dao.classPackage}.${dao.className};
import ${qo.importName};


/**
* <${service.describe}>
* @auth:DONNIE
*/
@Service
public class ${service.className} implements ${api.className} {
	private  Log log = LogFactory.getLog(${service.className}.class);

	@Autowired
	private ${dao.className} ${dao.classNameAttr};
	
	@Autowired
	private ${convertor.className} ${convertor.classNameAttr};

	@Override
	public ResponseDTO save(${model.className} obj) {
		// TODO Auto-generated method stub
		ResponseDTO responseDTO=new ResponseDTO();
		try {
			${dao.classNameAttr}.save(obj);
			responseDTO.setIsSuccess("T");
			responseDTO.setMsg("新增成功");
			responseDTO.setCode("00");
		} catch (Exception e) {
			log.error("",e);
			responseDTO.setIsSuccess("F");
			responseDTO.setMsg("系统异常:"+e.getMessage());
			responseDTO.setCode("99");
		}
		return responseDTO;
	}

	@Override
	public ResponseDTO update(${model.className} obj) {
		// TODO Auto-generated method stub
		ResponseDTO responseDTO=new ResponseDTO();
		try {
			${dao.classNameAttr}.update(obj);
			responseDTO.setIsSuccess("T");
			responseDTO.setMsg("修改成功");
			responseDTO.setCode("00");
		} catch (Exception e) {
			log.error("",e);
			responseDTO.setIsSuccess("F");
			responseDTO.setMsg("系统异常:"+e.getMessage());
			responseDTO.setCode("99");
		}
		return responseDTO;
	}

	@Override
	public ResponseDTO delete(${model.primaryKey.javaTypeName} ${model.primaryKey.attrName}) {
		// TODO Auto-generated method stub
		ResponseDTO responseDTO=new ResponseDTO();
		try {
	<#if 'Integer' == '${model.primaryKey.javaTypeName}' >
			${dao.classNameAttr}.delete(Integer.parseInt(${model.primaryKey.attrName}));
    <#elseif 'Long' == '${model.primaryKey.javaTypeName}'>
    		${dao.classNameAttr}.delete(Long.parseLong(${model.primaryKey.attrName}));
    <#elseif 'String' == '${model.primaryKey.javaTypeName}'>
    		${dao.classNameAttr}.delete(${model.primaryKey.attrName});
	</#if>
			responseDTO.setIsSuccess("T");
			responseDTO.setMsg("删除成功");
			responseDTO.setCode("00");
		} catch (Exception e) {
			log.error("",e);
			responseDTO.setIsSuccess("F");
			responseDTO.setMsg("系统异常:"+e.getMessage());
			responseDTO.setCode("99");
		}
		return responseDTO;
	}

	@Override
	public ${vo.className} queryById(${model.primaryKey.javaTypeName} ${model.primaryKey.attrName}) {
		// TODO Auto-generated method stub
		if(${model.primaryKey.attrName} == null){
			return null;
		}
		try {
			return ${convertor.classNameAttr}.convertorVO(${dao.classNameAttr}.query(${model.primaryKey.attrName}));
		} catch (Exception e) {
			log.error("",e);
		}
		return null;
	}
	
}
