package com.srcgen.utils;

import java.io.File;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

public class FileCreateUtils {

	public static void saveSrcToUtf8File(String filePathName,String srcCode) throws Exception{
		saveSrcToFile(filePathName, srcCode, "utf-8",true);
	}

	public static void saveSrcToFile(String filePathName,String srcCode,String encode) throws Exception{
		saveSrcToFile(filePathName, srcCode, encode,false);
	}
	
	public static void saveSrcToFile(String filePathName,String srcCode,String encode,boolean overwrite) throws Exception{
		File file = new File(filePathName);
		
		if( file.exists() && file.isFile() ){
			if(overwrite ==false){
				return;
			}
		}

		if(!file.exists()){
			file.getParentFile().mkdirs();
		}
		
		if(file.exists()){
			String code = FileUtils.readFileToString(file);
			if(StringUtils.equals(code,srcCode)){
				return;
			}
		}
		
		
		file.createNewFile();
		
		FileUtils.writeStringToFile(file, srcCode, encode);
		System.out.println(file.getPath());
	}

}
