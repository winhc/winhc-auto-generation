package com.srcgen.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * donnie
 */
public class StringEncoderUtils {


    public static String getEncoding(String str) {      
       String encode = "GB2312";      
      try {      
          if (str.equals(new String(str.getBytes(encode), encode))) {      //判断是不是GB2312
               String s = encode;      
              return s;      //是的话，返回“GB2312“，以下代码同理
           }      
       } catch (Exception exception) {      
       }      
       encode = "ISO-8859-1";      
      try {      
          if (str.equals(new String(str.getBytes(encode), encode))) {      //判断是不是ISO-8859-1
               String s1 = encode;      
              return s1;      
           }      
       } catch (Exception exception1) {      
       }      
       encode = "UTF-8";      
      try {      
          if (str.equals(new String(str.getBytes(encode), encode))) {   //判断是不是UTF-8
               String s2 = encode;      
              return s2;      
           }      
       } catch (Exception exception2) {      
       }      
       encode = "GBK";      
      try {      
          if (str.equals(new String(str.getBytes(encode), encode))) {      //判断是不是GBK
               String s3 = encode;      
              return s3;      
           }      
       } catch (Exception exception3) {      
       }      
       return "";
    }//如果都不是，说明输入的内容不属于常见的编码格式。
    
    
    public static String gb2312ToUtf8(String str) {

        String urlEncode = "" ;
        try {
            urlEncode = URLEncoder.encode (str, "UTF-8" );
        } catch (UnsupportedEncodingException e) {

            e.printStackTrace();

        }

        return urlEncode;

    }
    
    public static void main(String[] args) {
    	System.out.println(getEncoding("1%2C%E6%9C%8B%E5%8F%8B%E6%8E%A8%E8%8D%90%EF%BC%9B2.%E4%BB%8A%E6%97%A5%E5%A4%B4%E6%9D%A1%EF%BC%9B3+.%E5%BE%AE%E4%BF%A1%EF%BC%9B4.%E7%99%BE%E5%BA%A6%EF%BC%9B5.%E5%BE%AE%E5%8D%9A"));
		System.out.println(gb2312ToUtf8("�鈶悶�H釈6�冲"));
	}
}

