package com.srcgen.utils;

public class CamelCaseUtils {

    private static final char SEPARATOR = '_';


    /**
     * 首字母小写
     * @param s
     * @return
     */
    public static String toCamelCase(String s) {
        if (s == null) {
            return null;
        }

        s = s.toLowerCase();

        StringBuilder sb = new StringBuilder(s.length());
        boolean upperCase = false;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);

            if (c == SEPARATOR) {
                upperCase = true;
            } else if (upperCase) {
                sb.append(Character.toUpperCase(c));
                upperCase = false;
            } else {
                sb.append(c);
            }
        }

        return sb.toString();
    }

    public static String toCapitalizeCamelCase(String s) {
        if (s == null) {
            return null;
        }
        s = toCamelCase(s);
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    /**
     * 首字母大写
     * @param s
     * @return
     */
    public static String toCapitalize(String s) {
    	 if (s == null) {
             return null;
         }

         s = s.toLowerCase();

         StringBuilder sb = new StringBuilder(s.length());
         boolean upperCase = false;
         for (int i = 0; i < s.length(); i++) {
             char c = s.charAt(i);

             if (c == SEPARATOR) {
                 upperCase = true;
             } else if (upperCase) {
                 sb.append(Character.toUpperCase(c));
                 upperCase = false;
             } else {
                 sb.append(c);
             }
         }
        return sb.toString().substring(0, 1).toUpperCase() + sb.substring(1);
    }

    public static void main(String[] args) {
 /*      System.out.println(CamelCaseUtils.toUnderlineName("ISOCertifiedStaff"));
        System.out.println(CamelCaseUtils.toUnderlineName("CertifiedStaff"));
        System.out.println(CamelCaseUtils.toUnderlineName("UserID"));
        System.out.println(CamelCaseUtils.toCapitalizeCamelCase("USER_ID"));*/
        //use
       System.out.println(CamelCaseUtils.toCamelCase("iso_certified_staff"));
        System.out.println(CamelCaseUtils.toCamelCase("certified_staff"));
        System.out.println(CamelCaseUtils.toCamelCase("USER_ID"));
        System.out.println(CamelCaseUtils.toCamelCase("ID"));
  
        
        //use
        System.out.println(CamelCaseUtils.toCapitalize("USER_ID"));

    }

}

