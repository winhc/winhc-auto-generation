
package com.srcgen.domain;

import java.util.Map;

import com.srcgen.domain.entity.TableColumnsEntity;
import com.srcgen.utils.CamelCaseUtils;


public class TableColumnsDO extends TableColumnsEntity{

	private String attrName;

	/*************************/
	/**** 列对应的java类型名,如：Integer,Date,String... */
	private String javaTypeName;
	
	/**** 列对应的getter setter方法名,如：User_id,Create_time,Update_time... */
	private String domainName;

	public String getJavaTypeName() {
		return javaTypeName;
	}

	public void setJavaTypeName(String javaTypeName) {
		this.javaTypeName = javaTypeName;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}


	/***********************/
	public void formatAutoGen(Map<String,String> mysql2JavaTypeMap){
		//java type name
		javaTypeName=mysql2JavaTypeMap.get(getData_type());
		attrName = CamelCaseUtils.toCamelCase(this.getColumn_name());//字段中包含 _
	//	attrName = this.getColumn_name();//字段是驼峰
		domainName=CamelCaseUtils.toCapitalize(this.getAttrName());//首字母大写
		
	}

	public String getAttrName() {
		return attrName;
	}

	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}

	
}
