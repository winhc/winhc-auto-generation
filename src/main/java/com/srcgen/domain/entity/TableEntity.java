package com.srcgen.domain.entity;

public class TableEntity {
    private String table_schema;

    //表名,如:busi_org_user_apply
    private String table_name;

    //表注释
    private String table_comment;

    public String getTable_schema() {
        return table_schema;
    }

    public void setTable_schema(String table_schema) {
        this.table_schema = table_schema;
    }

    public String getTable_name() {
        return table_name;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }

    public String getTable_comment() {
        return table_comment;
    }

    public void setTable_comment(String table_comment) {
        this.table_comment = table_comment;
    }
}
