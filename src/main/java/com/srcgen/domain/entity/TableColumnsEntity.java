package com.srcgen.domain.entity;

/**
 *    表字段： SELECT * FROM information_schema.COLUMNS WHERE TABLE_NAME='sys_user' ORDER BY ORDINAL_POSITION ASC;
 */
public class TableColumnsEntity {

    private String table_schema;

    private String table_name;

    private String column_name;

    private String ordinal_position;

    private String is_nullable;

    private String data_type;

    private String character_maximum_length;

    private String column_type;

    private String column_key;

    private String column_comment;

    public String getTable_schema() {
        return table_schema;
    }

    public void setTable_schema(String table_schema) {
        this.table_schema = table_schema;
    }

    public String getTable_name() {
        return table_name;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }

    public String getColumn_name() {
        return column_name;
    }

    public void setColumn_name(String column_name) {
        this.column_name = column_name;
    }

    public String getOrdinal_position() {
        return ordinal_position;
    }

    public void setOrdinal_position(String ordinal_position) {
        this.ordinal_position = ordinal_position;
    }

    public String getIs_nullable() {
        return is_nullable;
    }

    public void setIs_nullable(String is_nullable) {
        this.is_nullable = is_nullable;
    }

    public String getData_type() {
        return data_type;
    }

    public void setData_type(String data_type) {
        this.data_type = data_type;
    }

    public String getCharacter_maximum_length() {
        return character_maximum_length;
    }

    public void setCharacter_maximum_length(String character_maximum_length) {
        this.character_maximum_length = character_maximum_length;
    }

    public String getColumn_type() {
        return column_type;
    }

    public void setColumn_type(String column_type) {
        this.column_type = column_type;
    }

    public String getColumn_key() {
        return column_key;
    }

    public void setColumn_key(String column_key) {
        this.column_key = column_key;
    }

    public String getColumn_comment() {
        return column_comment;
    }

    public void setColumn_comment(String column_comment) {
        this.column_comment = column_comment;
    }
}
