package com.srcgen;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import com.srcgen.builder.ParentConfig;
import com.srcgen.builder.TemplateMapBuilder;
import com.srcgen.dao.IAutoGenDAO;
import com.srcgen.domain.TableColumnsDO;
import com.srcgen.domain.entity.TableEntity;
import com.srcgen.utils.FileCreateUtils;

import freemarker.template.Template;

public class GenHelper {
	
	
	/**
	 * 
	 * @param projectRoot   系统地址 例如 F:/git-service
	 * @param tableName     表名  ：  CUSTOM_LINKMAN
	 * @param projectName   项目名 ： winhc-mis-back
	 * @param dbName        数据库名 :firefly
	 * @param sysPackageName  保存的包名：  com.winhc.mis
	 * @param sysSaveDir     保存的文件地址  /src/main/java/com/winhc/mis
	 * @param sysAutoModel  包名的地址  caseBaseInfo
	 * @throws Exception
	 */
	public static void genCodes(String projectRoot,String tableName,String projectName
			,String dbName
			,String sysPackageName
			,String sysSaveDir
			,String sysAutoModel) throws Exception {
		
		BeanFactory beanFactory=new ClassPathXmlApplicationContext(new String[] {
				"dao/autogen-mybatis-datasource.xml",
				"dao/autogen-mybatis-dao.xml",
				"root-context.xml"
				});
		
		
		System.out.println("..SRC AUTO GEN START..");
		
		IAutoGenDAO iAutoGenDAO=(IAutoGenDAO)beanFactory.getBean("iAutoGenDAO");
	
		FreeMarkerConfigurer freemarkerConfig=(FreeMarkerConfigurer)beanFactory.getBean("freemarkerConfig");
		//freemarkerConfig.getConfiguration().
		//foreach table name
		TableEntity tableProfileDO=null;
		List<TableColumnsDO> tableColumnsDOList=null;
		
		//查询表
		tableProfileDO=iAutoGenDAO.findTableProfile(tableName,dbName);
		//查询表字段
		tableColumnsDOList=iAutoGenDAO.listTableColumns(tableName,dbName);

         Map<String,ParentConfig> map = new TemplateMapBuilder()
				.setProjectRoot(projectRoot)
				.setTableName(tableName)
				.setProjectName(projectName)
				.setTableInfo(tableProfileDO)
				.setColumnList(tableColumnsDOList)
				.setSysPackageName(sysPackageName)
				.setSysSaveDir(sysSaveDir)
				.setSysAutoModel(sysAutoModel)
				.build();
		for(String configKey:map.keySet()){
			ParentConfig config = map.get(configKey);
			Template template = freemarkerConfig.getConfiguration().getTemplate(config.getTemplateDir());
			String tempStr=FreeMarkerTemplateUtils.processTemplateIntoString(template, map);
			//System.out.println(tempStr);

			FileCreateUtils.saveSrcToUtf8File(config.getSaveDir()+config.getClassName()+ ".java", tempStr);
		}
		System.out.println("..SRC AUTO GEN  END ..");
	}
	
	
	public static void main(String[] args) {
		try {
			genCodes("F:/idea-project-all/flysky/microservice", 
					"ASSETS_REWARD_FAVOR", "property-clue-consult",
					"firefly", "com.winhc.flysky.property.api",
					"/src/main/java/com/winhc/flysky/property/api"
					,"assetsReward");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
