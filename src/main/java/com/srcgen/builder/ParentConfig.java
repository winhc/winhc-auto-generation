package com.srcgen.builder;

import java.io.Serializable;


public abstract class ParentConfig implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
     * 包名
     */
    protected String classPackage;
    /**
     * 文件存储路径
     */
    protected String saveDir;
    /**
     * 模板所在位置
     */
    protected String templateDir;
    /**
     * 描述
     */
    private String describe;

    /**
     * 方法名称
     */
    private String methodDomain;
    /**
     * 类名称,首字母大写的驼峰==domain
     */
    protected String className;

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getMethodDomain() {
        return methodDomain;
    }

    public void setMethodDomain(String methodDomain) {
        this.methodDomain = methodDomain;
    }

    public abstract String getClassPackage();

    public void setClassPackage(String classPackage) {
        this.classPackage = classPackage;
    }

    public abstract String getSaveDir();

    public void setSaveDir(String saveDir) {
        this.saveDir = saveDir;
    }

    public abstract String getTemplateDir() ;

    public void setTemplateDir(String templateDir) {
        this.templateDir = templateDir;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
    /**
     * 类引用名称，驼峰=attr
     */
    public String getClassNameAttr() {
        return className.substring(0,1).toLowerCase()+className.substring(1);
    }

    /**
     * 导入包名称
     */
    public String getImportName() {
        return classPackage+"."+className;
    }

}
