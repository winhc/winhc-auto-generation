package com.srcgen.config;

import com.srcgen.builder.ParentConfig;
import org.apache.commons.lang3.StringUtils;


/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @auth:DONNIE
 * @see: [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class ApiConfig extends ParentConfig {
/*    //包所在位置
    private static final String DefaultClassPackage = "com.winhc.mis.service.${autoModel}.impl";
    //保存路径
    private static final String DefaultSaveDir = "/src/main/java/com/winhc/mis/service/${autoModel}/impl/";*/
    
    //包所在位置
    private static final String DefaultClassPackage = "${packageName}.service.${autoModel}.impl";
    //保存路径
    private static final String DefaultSaveDir = "${saveDir}/service/${autoModel}/impl/";
    
    //模板所在位置
    private static final String DefaultTemplateDir = "api.ftl";

    @Override
    public String getClassPackage() {
        return StringUtils.defaultIfEmpty(classPackage,DefaultClassPackage);
    }

    @Override
    public String getSaveDir() {
        return StringUtils.defaultIfEmpty(saveDir,DefaultSaveDir);
    }

    @Override
    public String getTemplateDir() {
        return StringUtils.defaultIfEmpty(templateDir,DefaultTemplateDir);
    }
}
