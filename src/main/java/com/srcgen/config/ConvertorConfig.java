package com.srcgen.config;

import org.apache.commons.lang3.StringUtils;

import com.srcgen.builder.ParentConfig;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @auth:DONNIE
 * @see: [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class ConvertorConfig extends ParentConfig {
	 //包所在位置
    private String DefaultClassPackage = "${packageName}.service.${autoModel}.vo";
    //保存路径
    private String DefaultSaveDir = "${saveDir}/service/${autoModel}/vo/";
    //模板所在位置
    private String DefaultTemplateDir = "convertor.ftl";

    @Override
    public String getClassPackage() {
        return StringUtils.defaultIfEmpty(classPackage, DefaultClassPackage);
    }

    @Override
    public String getSaveDir() {
        return StringUtils.defaultIfEmpty(saveDir,DefaultSaveDir);
    }

    @Override
    public String getTemplateDir() {
        return StringUtils.defaultIfEmpty(templateDir,DefaultTemplateDir);
    }
}
