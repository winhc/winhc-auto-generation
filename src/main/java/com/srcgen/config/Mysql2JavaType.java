package com.srcgen.config;

import org.apache.commons.lang3.StringUtils;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @auth:DONNIE
 * @see: [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public enum Mysql2JavaType {
//    <entry key="double" value="Double" />
//    <entry key="int" value="Integer" />
//    <entry key="tinyint" value="Integer" />
//    <entry key="datetime" value="Date" />
//    <entry key="timestamp" value="Date" />
//    <entry key="varchar" value="String" />
//    <entry key="text" value="String" />
//    <entry key="bigint" value="Long" />
//    <entry key="decimal" value="BigDecimal"/>
    DOUBLE("double","Double"),
    INT("int","Integer"),
    TINYINT("tinyint","Integer"),
    DATE("datetime","Date"),
    StringType("varchar","String"),
    TEXT("text","String"),
    BIGINT("bigint","Long"),
    DECIMAL("decimal","Double"),
    INTEGER("smallint","Integer"),
	CHAR("char","String");
	
    Mysql2JavaType(String mysqlType,String javaType){
        this.mysqlType = mysqlType;
        this.javaType = javaType;
    }

    private String mysqlType;

    private String javaType;

    public static String getJavaTypeFromMysql(String mysqlType) {
        for(Mysql2JavaType type:Mysql2JavaType.values()){
            if(StringUtils.equals(mysqlType,type.mysqlType)){
                return type.javaType;
            }
        }
        return null;
    }
}
