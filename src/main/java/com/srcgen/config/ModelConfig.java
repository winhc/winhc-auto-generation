package com.srcgen.config;

import com.srcgen.builder.ParentConfig;
import com.srcgen.domain.TableColumnsDO;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @auth:DONNIE
 * @see: [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class ModelConfig extends ParentConfig {
    //包所在位置
    private String DefaultClassPackage = "${packageName}.service.${autoModel}.model";
    //保存路径
    private String DefaultSaveDir = "${saveDir}/service/${autoModel}/model/";
    //模板所在位置
    private String DefaultTemplateDir = "table_to_model.ftl";

    private List<TableColumnsDO> columnsList;

    private TableColumnsDO primaryKey;

    public List<TableColumnsDO> getColumnsList() {
        return columnsList;
    }

    public void setColumnsList(List<TableColumnsDO> columnsList) {
        this.columnsList = columnsList;
    }

    public TableColumnsDO getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(TableColumnsDO primaryKey) {
        this.primaryKey = primaryKey;
    }

    @Override
    public String getClassPackage() {
        return StringUtils.defaultIfEmpty(classPackage, DefaultClassPackage);
    }

    @Override
    public String getSaveDir() {
        return StringUtils.defaultIfEmpty(saveDir,DefaultSaveDir);
    }

    @Override
    public String getTemplateDir() {
        return StringUtils.defaultIfEmpty(templateDir,DefaultTemplateDir);
    }
}
