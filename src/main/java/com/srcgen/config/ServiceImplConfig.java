package com.srcgen.config;

import com.srcgen.builder.ParentConfig;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @auth:DONNIE
 * @see: [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class ServiceImplConfig extends ParentConfig {
    private static ServiceImplConfig serviceImplConfig;

    //serviceImpl中查询参数
    private List<String> queryParams = new ArrayList(){{
        add("status");
        add("warehouseId");
        add("keyword");
    }};
    //包所在位置
    private String DefaultClassPackage = "${packageName}.service.${autoModel}.impl";
    //保存路径
    private String DefaultSaveDir = "${saveDir}/service/${autoModel}/impl/";
    //模板所在位置
    private String DefaultTemplateDir = "service.ftl";

    private String primaryKey;

    public String getPrimaryKey() {
        return primaryKey;
    }

    public String getPrimaryKeyDomain(){
        return primaryKey.substring(0,1).toUpperCase()+primaryKey.substring(1);
    }

    public void setPrimaryKey(String primaryKey) {
        this.primaryKey = primaryKey;
    }

    @Override
    public String getClassPackage() {
        return StringUtils.defaultIfEmpty(classPackage, DefaultClassPackage);
    }

    @Override
    public String getSaveDir() {
        return StringUtils.defaultIfEmpty(saveDir,DefaultSaveDir);
    }

    @Override
    public String getTemplateDir() {
        return StringUtils.defaultIfEmpty(templateDir,DefaultTemplateDir);
    }

    public List<String> getQueryParams() {
        return queryParams;
    }

    public void setQueryParams(List<String> queryParams) {
        this.queryParams = queryParams;
    }

    public static ServiceImplConfig builder(List<String> queryParams){
        serviceImplConfig = new ServiceImplConfig();
        if(CollectionUtils.isNotEmpty(queryParams)){
            serviceImplConfig.queryParams.addAll(queryParams);
        }
        return serviceImplConfig;
    }
}
