package com.srcgen.config;

import com.srcgen.builder.ParentConfig;
import com.srcgen.domain.TableColumnsDO;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * <一句话功能简述>
 * <功能详细描述>
 *
 * @auth:DONNIE
 * @see: [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class VoConfig extends ParentConfig {
    //包所在位置
    private String DefaultClassPackage = "${packageName}.service.${autoModel}.vo";
    //保存路径
    private String DefaultSaveDir = "${saveDir}/service/${autoModel}/vo/";
    //模板所在位置
    private String DefaultTemplateDir = "table_to_vo.ftl";

    
    //serviceImpl中查询参数
    private List<TableColumnsDO> queryParams = new ArrayList();

    public List<TableColumnsDO> getQueryParams() {
        return queryParams;
    }

    public void setQueryParams(List<TableColumnsDO> queryParams) {
        for(TableColumnsDO columns:queryParams){
            if( "status".equals(columns.getAttrName())
                    ||"char(1)".equals(columns.getColumn_type())
                    || StringUtils.isNotBlank(columns.getColumn_comment())){
                this.queryParams.add(columns);
            }
        }
    }

    @Override
    public String getClassPackage() {
        return StringUtils.defaultIfEmpty(classPackage, DefaultClassPackage);
    }

    @Override
    public String getSaveDir() {
        return StringUtils.defaultIfEmpty(saveDir,DefaultSaveDir);
    }

    @Override
    public String getTemplateDir() {
        return StringUtils.defaultIfEmpty(templateDir,DefaultTemplateDir);
    }
}
