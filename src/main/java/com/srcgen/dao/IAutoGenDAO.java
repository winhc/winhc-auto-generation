package com.srcgen.dao;

import java.util.List;

import com.srcgen.domain.TableColumnsDO;
import com.srcgen.domain.entity.TableEntity;

public interface IAutoGenDAO {
	
	TableEntity findTableProfile(String tableName, String dbName);
	
	List<TableColumnsDO> listTableColumns(String tableName, String dbName);

}
