package com.srcgen.dao.mybatis;

import org.apache.ibatis.session.SqlSession;


public class BaseDAOImp {
	
	/**
     * injiect.
     */
    protected SqlSession sqlSession = null;
    
    /**
     * setSqlSession.
     * 
     * @param sqlSession
     *            the sqlSession to set
     */
    public void setSqlSession(SqlSession sqlSession) {
        this.sqlSession = sqlSession;
    }
    
    /**
     * 分页查询模版方法.
     * 
     * @param parameter
     *            查询List的sqlMap 查询总条数的sqlMap=pageListSqlMap+"_count"
     * @param dto
     *            DTO
     * @param pageListSqlMap
     *            SQL
     * @param <T>
     *            T
     * @return 返回
     */
//    public <T extends PagingProperties> PagingAlgorithm<T> page(
//            PagingProperties parameter, T dto, String pageListSqlMap) {
//        PagingAlgorithm<T> paging = new PagingAlgorithm<T>();
//        paging.setPagingProperties(parameter);
//        paging.copyPagingProperties(dto);
//        Integer rowCount = sqlSession.selectOne(pageListSqlMap + "_count", dto);
//        if (rowCount == null || rowCount == 0) {
//            paging.setTotalItem(0);
//            paging.setPageList(Collections.<T> emptyList());
//            return paging;
//        }
//        paging.setTotalItem(rowCount);
//        paging.copyPagingProperties(dto);
//        List<T> returnDate = sqlSession.selectList(pageListSqlMap, dto);
//        paging.setPageList(returnDate);
//        return paging;
//    }

}
