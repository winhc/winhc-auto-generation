package com.srcgen.dao.mybatis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.srcgen.dao.IAutoGenDAO;
import com.srcgen.domain.TableColumnsDO;
import com.srcgen.domain.entity.TableEntity;


public class AutoGenDAOImpl extends BaseDAOImp implements IAutoGenDAO {


	@Override
	public TableEntity findTableProfile(String tableName, String dbName) {
		Map<String,String> paramMap = new HashMap<>();
		paramMap.put("tableName",tableName);
		paramMap.put("dbName",dbName);
		return sqlSession.selectOne("map.AutoGen_SqlMap.table_profile_find", paramMap);
	}

	@Override
	public List<TableColumnsDO> listTableColumns(String tableName, String dbName) {
		Map<String,String> paramMap = new HashMap<>();
		paramMap.put("tableName",tableName);
		paramMap.put("dbName",dbName);
		List<TableColumnsDO> list =sqlSession.selectList("map.AutoGen_SqlMap.table_columns_list", paramMap);

		return list;
	}

}
